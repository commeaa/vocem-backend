const mongoose = require("mongoose");

/**
 * TODO Faire un back office pour les restaurateurs
 */

const Schema = mongoose.Schema;
const restaurantSchema = new Schema(
  {
    image: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
      unique: true,
    },
    description: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    dishes: [
      {
        type: Schema.Types.ObjectId,
        ref: "Dish",
      },
    ],
    address: {
      type: Schema.Types.ObjectId,
      ref: "Address",
    },
    likes: {
      type: Number,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Restaurant", restaurantSchema);
