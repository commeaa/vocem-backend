const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const addressSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    streetName: {
      type: String,
      required: true,
    },
    zipCode: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    country: {
      type: String,
      required: true,
    },
    comments: {
      type: String,
    },
    default: {
      type: Boolean,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Address", addressSchema);
