const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./address.model");

const userSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
    },
    name: {
      type: String,
    },
    phone: {
      type: Number,
    },
    favorite: {
      type: Schema.Types.ObjectId,
      ref: "Restaurant",
    },
    liked: [
      {
        type: Schema.Types.ObjectId,
        ref: "Restaurant",
      },
    ],
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    addresses: [
      {
        type: Schema.Types.ObjectId,
        ref: "Address",
      },
    ],
    restaurantIds: [
      {
        type: Schema.Types.ObjectId,
        ref: "Restaurant",
      },
    ],
    langage: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
