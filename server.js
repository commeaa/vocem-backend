const express = require("express");
const mongoose = require("mongoose");
const { ApolloServer, makeExecutableSchema } = require("apollo-server");

const { typeDefs } = require("./graphql/schema");
const { resolvers } = require("./graphql/resolvers");
const { getUser } = require("./graphql/resolvers/utils");

const app = express();
//const uri = `mongodb+srv://Fabrice:MotdepasseTresCompliqué92140@vocemcluster.9mrx4.mongodb.net/vocem?retryWrites=true&w=majority`;
const uri = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@vocemcluster.9mrx4.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`;
const options = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
mongoose.connect(uri, options).catch((error) => {
  throw error;
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers.authorization || "";
    const user = await getUser(token);
    // add the user to the context
    return { user };
  },
  introspection: true,
  playground: true,
});

server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
  console.log(`
    🚀  Server is ready at ${url}
  `);
});
