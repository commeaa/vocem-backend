const { gql } = require("apollo-server");

const mutation = gql`
  type Mutation {
    login(username: String!, password: String!): User!
    signup(username: String!, email: String!, password: String!): User!
    modifyUser(input: UserInput!): User
    addAddress(input: AddressInput!): Address!
    removeAddress(id: String!): String!

    createDish(input: DishInput!): Dish!
    deleteDish(id: String!): Dish!

    createAddress(input: AddressInput!): Address
    editAddress(input: AddressInput!): Address
    deleteAddress(id: String!): Address!

    createRestaurant(input: AddRestaurantInput!): Restaurant
    editRestaurant(id: String!, input: UpdateRestaurantInput!): Restaurant!
    addDish(restaurantId: String!, input: DishInput!): Dish!
    removeDish(id: String!, restaurantId: String!): Dish!
    deleteRestaurant(id: String!): Restaurant!
  }
`;

module.exports = {
  mutation,
};
