const dishType = require("./dishType");
const userType = require("./userType");
const orderType = require("./orderType");
const addressType = require("./addressType");
const restaurantType = require("./restaurantType");

module.exports = {
  userType,
  dishType,
  restaurantType,
  addressType,
  orderType,
};
