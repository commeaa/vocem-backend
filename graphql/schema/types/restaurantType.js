const { gql } = require("apollo-server");
/**
 * TODO Un restaurant pourrais avoir un owner ?
 * TODO Update les roles, un owner de restaurant peut modifié son restaurant
 */
module.exports = gql`
  enum FoodType {
    ASIAN
    ITALIAN
    BURGER
    BAR
  }

  type Restaurant {
    _id: ID!
    image: String!
    name: String!
    description: String!
    dishes: [Dish]
    phone: String!
    type: String!
    address: Address!
    likes: Int!
  }

  input AddRestaurantInput {
    image: String!
    name: String!
    description: String!
    phone: String!
    likes: Int = 0
    type: FoodType!
    address: AddressInput!
  }

  input UpdateRestaurantInput {
    name: String
    description: String
    phone: String
    likes: Int = 0
    type: FoodType
    address: AddressInput
  }
`;
