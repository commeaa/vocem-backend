const { gql } = require("apollo-server");

module.exports = gql`
  enum addressType {
    CLIENT
    RESTAURANT
  }

  type Address {
    _id: ID!
    name: String!
    streetName: String!
    zipCode: String
    city: String
    country: String
    comments: String
    default: Boolean
  }

  input AddressInput {
    name: String
    streetName: String!
    zipCode: String!
    city: String!
    country: String!
    comments: String
  }
`;
