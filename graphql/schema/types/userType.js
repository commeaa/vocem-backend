const { gql } = require("apollo-server");

module.exports = gql`
  enum Role {
    ADMIN
    CLIENT
    RESTORER
  }

  type UserAuth {
    username: String!
    email: String!
    token: String!
  }

  interface User {
    _id: ID!
    username: String!
    token: String
    name: String!
    email: String!
    phone: Int
    langage: String
    role: Role
  }

  type Client implements User {
    _id: ID!
    username: String!
    token: String
    name: String!
    email: String!
    phone: Int
    favorite: [Restaurant]
    langage: String
    addresses: [Address]
    role: Role
    cart: String
    clientField: String
  }

  type RestaurantOwner implements User {
    _id: ID!
    username: String!
    token: String
    name: String!
    email: String!
    phone: Int
    langage: String
    address: Address
    role: Role
    restaurantIds: [String]
  }

  input UserInput {
    name: String
    password: String
    email: String
    phone: Int
    username: String
    langage: String
  }
`;
