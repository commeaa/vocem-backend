const { gql } = require("apollo-server");

module.exports = gql`
  type Order {
    _id: ID!
    name: String!
    status: String!
    score: Int!
    dishes: [Dish]!
    price: Int!
  }
`;
