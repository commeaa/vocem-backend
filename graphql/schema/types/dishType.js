const { gql } = require("apollo-server");

module.exports = gql`
  enum DishType {
    NOTYPE
    MEAL
    DESSERT
    STARTER
    DRINK
    SNACK
  }

  type Dish {
    _id: ID!
    image: String!
    name: String!
    description: String
    type: DishType
    price: Float
  }

  input DishInput {
    name: String!
    price: Float!
    description: String!
    type: DishType!
  }

  input DishFilter {
    restaurantId: String
    type: DishType
    search: String
  }
`;
