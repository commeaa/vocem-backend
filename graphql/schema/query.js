const { gql } = require("apollo-server");

const query = gql`
  type Query {
    user: User
    users: [User]!
    me: User!

    addresses: [Address]

    dishes(filter: DishFilter): [Dish!]

    restaurants: [Restaurant]
    restaurant(id: String!): Restaurant
  }
`;

module.exports = {
  query,
};
