const { mergeTypeDefs } = require("@graphql-tools/merge");

const { query } = require("./query");
const { mutation } = require("./mutation");

const {
  userType,
  dishType,
  addressType,
  restaurantType,
  orderType,
} = require("./types");

const typeDefs = [
  query,
  mutation,
  dishType,
  userType,
  addressType,
  restaurantType,
  orderType,
];

module.exports = { typeDefs };
