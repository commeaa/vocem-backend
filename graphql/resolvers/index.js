const dishesResolvers = require("./dishesResolvers");
const usersResolvers = require("./usersResolvers");
const addressesResolvers = require("./addressesResolvers");
const restaurantResolvers = require("./restaurantResolvers");

const resolvers = [
  dishesResolvers,
  usersResolvers,
  addressesResolvers,
  restaurantResolvers,
];
module.exports = { resolvers };
