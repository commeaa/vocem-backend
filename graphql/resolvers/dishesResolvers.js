const Dish = require("../../models/dish.model");
const Restaurant = require("../../models/restaurant.model");
const _ = require("lodash");
const {
  UserInputError,
  AuthenticationError,
  ValidationError,
} = require("apollo-server");

module.exports = {
  Query: {
    dishes: async (parent, { filter }, context) => {
      //  if (!context.user) throw new AuthenticationError("You must be logged in");
      try {
        let restaurantQuery = {};
        let searchQuery = {};
        let typeQuery = {};

        if (filter?.restaurantId) {
          const restaurant = await Restaurant.findOne({
            _id: filter.restaurantId,
          });
          restaurantQuery = { _id: { $in: restaurant.dishes } };
        }
        if (filter?.type) {
          typeQuery = { type: filter.type };
        }
        if (filter?.search) {
          searchQuery = {
            name: new RegExp(`${filter.search}`, "i"),
          };
        }
        const dishFetched = await Dish.find({
          ...restaurantQuery,
          ...typeQuery,
          ...searchQuery,
        });
        return dishFetched.map((dish) => {
          return dish;
        });
      } catch (error) {
        throw error;
      }
    },
  },
  Mutation: {
    createDish: async (_, { input }) => {
      try {
        const oui = new Dish({
          ...input,
        });
        const newdish = await oui.save();
        return { newdish };
      } catch (error) {
        throw error;
      }
    },
    deleteDish: async (_, { id }) => {
      const parent = await Restaurant.findOne({ dishes: { _id: id } });
      if (parent)
        throw new UserInputError(
          `Ce repas est utilisée par le restaurant "${parent.name}".`
        );
      else {
        const deletedDish = await Dish.findById(id);
        if (deletedDish) {
          deletedDish.deleteOne();
        } else throw new UserInputError(`Ce repas n'existe pas`);
        return deletedDish;
      }
    },
  },
};
