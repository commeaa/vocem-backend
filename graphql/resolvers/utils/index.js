const jwt = require("jsonwebtoken");
const { SECRET } = require("../../../config");

const getUser = async (auth) => {
  if (!auth) return null;

  const token = auth.split("Bearer ")[1];
  if (!token) return null;
  const user = await jwt.verify(token, SECRET, (err, decoded) => {
    if (err) return null;
    return decoded;
  });
  return user;
};

module.exports = { getUser };
