const User = require("../../models/user.model.js");
const Address = require("../../models/address.model.js");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { getUser } = require("./utils");

const {
  UserInputError,
  AuthenticationError,
  ValidationError,
} = require("apollo-server");

const { SECRET } = require("../../config");

const validateRegister = ({ username, email, password }) => {
  if (username !== "" && email !== "" && password !== "") {
    return { errors: null, valid: true };
  } else return { errors: "Fields must not be empty.", valid: false };
};

const getToken = ({ id, username, email }) =>
  jwt.sign(
    {
      id,
      username,
      email,
    },
    SECRET,
    { expiresIn: "1d" }
  );

module.exports = {
  User: {
    __resolveType(user, context, info) {
      if (user.testingField) {
        return "RestaurantOwner";
      } else {
        return "Client";
      }
      return null;
    },
  },
  Query: {
    users: async (parent, args, context) => {
      try {
        const usersFetched = await User.find().populate("address");

        return usersFetched.map((user) => {
          return {
            ...user._doc,
            _id: user.id,
            createdAt: user._doc.createdAt
              ? new Date(user._doc.createdAt).toISOString()
              : new Date().toISOString(),
          };
        });
      } catch (error) {
        throw error;
      }
    },
    user: async (parent, [id], context) => {
      try {
        const usersFetched = await User.findOne({ _id: id }).populate(
          "address"
        );
        console.log(usersFetched);
        return usersFetched.map((user) => {
          return user;
        });
      } catch (error) {
        throw error;
      }
    },

    async me(_, args, context) {
      const user = context.user;
      if (!user) throw new AuthenticationError("You must be logged in");
      const userDb = await User.findById(user.id).populate("addresses");
      console.log(userDb);
      return userDb;
    },
  },
  Mutation: {
    async login(_, { username, password }) {
      //   const { errors, valid } = validateLogin(username, password);
      //   if (!valid) throw new UserInputError("Error", { errors });

      console.log("username :", username, "\npassword:", password);

      const user = await User.findOne({ username });
      if (!user) throw new AuthenticationError("this user is not found!");

      const match = await bcrypt.compare(password, user.password);
      if (!match) throw new AuthenticationError("wrong password!");

      const token = getToken(user);
      return {
        id: user._id,
        ...user._doc,
        token,
      };
    },
    async signup(_, { username, email, password }) {
      console.log(username, email, password);

      const { errors, valid } = validateRegister(username, password, email);

      if (!valid) throw new UserInputError("Error", { errors });

      const user = await User.findOne({ username });
      if (user) throw new ValidationError("This username already taken !");

      password = await bcrypt.hash(password, 10);
      const newUser = new User({
        username,
        password,
        email,
        role: "CLIENT",
      });
      const res = await newUser.save();
      const token = getToken(res);

      return {
        id: res._id,
        ...res._doc,
        token,
      };
    },
    async modifyUser(_, { input }, context) {
      try {
        const user = context.user;
        if (!user) throw new AuthenticationError("You must be logged in");
        let encryptedPassword;
        const userObject = await User.findOne({ _id: user.id });

        if (input.password)
          encryptedPassword = await bcrypt.hash(input.password, 10);
        const editedUser = User.findByIdAndUpdate(
          { _id: user.id },
          {
            ...input,
            password: input.password ? encryptedPassword : user.password,
          },
          function (err, result) {
            if (err) {
              return err;
            } else {
              return result;
            }
          }
        ).populate("address");
        return editedUser;
      } catch (error) {
        throw error;
      }
    },
    async addAddress(_, { input }, context) {
      try {
        const user = context.user;
        if (!user) throw new AuthenticationError("You must be logged in");

        const userObject = await User.findOne({ _id: user.id });

        let address = new Address({
          ...input,
          default: userObject.address.length === 0 ? true : false,
        });

        userObject.address.push(address);
        userObject.save();

        await address.save();
        return address;
      } catch (error) {
        throw error;
      }
    },
    async removeAddress(_, { id }, context) {
      try {
        const user = context.user;
        if (!user) throw new AuthenticationError("You must be logged in");
        const isAddressExist = await Address.exists({ _id: id });

        if (!isAddressExist) {
          throw new UserInputError("Cette addresse n'existe pas");
        }

        const addressToRemove = await Address.findOne({ _id: id });
        const userObject = await User.findOne({ _id: user.id });
        let arrayAddresses = userObject.address.filter(
          (address) => address != id
        );
        if (arrayAddresses.length > 0) {
          const addressToDefault = await Address.findByIdAndUpdate(
            { _id: arrayAddresses[0] },
            { default: true }
          );
        }
        userObject.address = arrayAddresses;
        userObject.save();
        addressToRemove.deleteOne();
        return "Addresse supprimée.";
      } catch (error) {
        throw error;
      }
    },
  },
};
