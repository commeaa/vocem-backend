const Restaurant = require("../../models/restaurant.model");
const User = require("../../models/user.model");
const Address = require("../../models/address.model");
const Dish = require("../../models/dish.model");

const {
  ApolloError,
  UserInputError,
  AuthenticationError,
  ValidationError,
} = require("apollo-server");

module.exports = {
  Query: {
    restaurants: async (parent, args, context) => {
      const restaurants = await Restaurant.find({})
        .populate("address")
        .populate("dishes");
      return restaurants;
    },
    restaurant: async (parent, { id }, context) => {
      const restaurant = await Restaurant.findOne({ _id: id })
        .populate("address")
        .populate("dishes");

      return restaurant;
    },
  },
  Mutation: {
    createRestaurant: async (_, { input }) => {
      try {
        const address = new Address({
          ...input.address,
          name: input.address.name ? input.address.name : "Principale",
          default: true,
        });
        const restaurant = new Restaurant({
          ...input,
          address: address._id,
        });
        await restaurant.save();
        await address.save();
        return restaurant;
      } catch (error) {
        throw error;
      }
    },
    addDish: async (_, { restaurantId, input }, context) => {
      try {
        /**
         * TODO L'utilisateur doit etre admin ou l'owner du restaurant
         */
        // const user = context.user;
        // if (!user) throw new AuthenticationError("You must be logged in");

        const restaurantObject = await Restaurant.findOne({
          _id: restaurantId,
        });
        // const dishObject = await Dish.findOne({ _id: dishId });

        if (!restaurantObject)
          throw new UserInputError("Ce restaurant n'existe pas.");

        const newDish = new Dish({
          ...input,
        });
        const newdish = await newDish.save();

        restaurantObject.dishes.push(newDish);
        await restaurantObject.save();

        return newDish;
      } catch (error) {
        throw error;
      }
    },
    editRestaurant: async (_, { id, input }) => {
      try {
        const { address, ...restaurantNewData } = input;
        const newRestaurant = await Restaurant.findOneAndUpdate(
          { _id: id },
          { ...restaurantNewData },
          { new: true },
          function (err, result) {
            if (err) return err;
            Address.findOneAndUpdate(
              { _id: result.address._id },
              { ...address },
              { new: true },
              function (err, result) {
                if (err) return err;
              }
            );
          }
        ).populate("address");
        /**
         * TODO Faire les validators pour vérifier que les données sont bonnes
         */
        return newRestaurant;
      } catch (error) {
        throw error;
      }
    },
    deleteRestaurant: async (_, { id }) => {
      const deletedRestaurant = await Restaurant.findById(id);

      if (deletedRestaurant) {
        if (deletedRestaurant.address) {
          const deletedAddress = await Address.findById(
            deletedRestaurant.address
          );
          deletedAddress.deleteOne();
        }
        deletedRestaurant.deleteOne();
        return deletedRestaurant;
      } else return null;
    },
  },
};
