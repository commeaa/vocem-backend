const Address = require("../../models/address.model");
const Restaurant = require("../../models/restaurant.model");
const User = require("../../models/user.model");

const { UserInputError } = require("apollo-server");

module.exports = {
  Query: {
    addresses: async (parent, args, context) => {
      const addresses = await Address.find({});
      return addresses;
    },
  },
  Mutation: {
    addAddress: async (_, { input }, context) => {
      try {
        const user = context.user;
        if (!user) throw new AuthenticationError("You must be logged in");

        /**
         * TODO Faire les validators pour vérifier que les données sont bonnes
         * TODO Faire la partie restaurant
         */

        const userObject = await User.findById(user.id);
        const address = new Address({
          ...input,
          default: userObject.addresses.length === 0 ? true : false,
        });

        userObject.addresses.push(address);
        userObject.save();

        const newAddress = await address.save();
        return newAddress;
      } catch (error) {
        throw error;
      }
    },
    editAddress: async (_, { input }) => {
      try {
        const newAddress = Address.findByIdAndUpdate(
          { _id: input.id },
          { ...input },
          function (err, result) {
            if (err) {
              return err;
            } else {
            }
          }
        );
        /**
         * TODO Faire les validators pour vérifier que les données sont bonnes
         */
        return newAddress;
      } catch (error) {
        throw error;
      }
    },
    deleteAddress: async (_, { id }) => {
      const parent = await Restaurant.findOne({ address: { _id: id } });
      if (parent)
        throw new UserInputError(
          `Cette addresse est utilisée par le restaurant "${parent.name}".`
        );
      else {
        const deletedAddress = await Address.findById(id);
        if (deletedAddress) {
          deletedAddress.deleteOne();
        } else throw new UserInputError(`Cette addresses n'existe pas`);
        return deletedAddress;
      }
    },
  },
};
